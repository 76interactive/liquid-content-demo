<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2017-03-20T10:52:40+01:00
 *
 * DO NOT EDIT THIS FILE DIRECTLY
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'site' => 'Liquid content demo',
    'version_installed' => '5.7.5.9',
    'misc' => array(
        'access_entity_updated' => 1489761589,
        'latest_version' => '5.7.5.13',
        'do_page_reindex_check' => false,
    ),
    'seo' => array(
        'canonical_url' => '',
        'canonical_ssl_url' => '',
        'redirect_to_canonical_url' => 0,
        'url_rewriting' => 1,
    ),
    'editor' => array(
        'concrete' => array(
            'enable_filemanager' => '1',
            'enable_sitemap' => '1',
        ),
        'plugins' => array(
            'selected' => array(
                'undoredo',
                'underline',
                'concrete5lightbox',
                'specialcharacters',
                'table',
                'fontfamily',
                'fontsize',
                'fontcolor',
            ),
        ),
    ),
);
