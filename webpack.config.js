'use strict';

const path = require('path');

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const themeDirectory = '/packages/liquid_content_demo/themes/liquid_content_demo';

const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    app: path.join(__dirname, themeDirectory, '/assets/js/app.js'),
  },
  output: {
    path: path.join(__dirname, themeDirectory, 'dist'),
    filename: '/js/[name].js',
    publicPath: path.join(
      '/',
      themeDirectory,
      'dist'
    ),
    pathinfo: true,
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(css|scss)$/,
        loader: ExtractTextPlugin.extract([
          'css?sourceMap',
          'postcss',
          'sass?sourceMap',
        ]),
      },
      {
        test: /fonts\/[^.]+\.(ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file?name=/fonts/[hash].[ext]",
      },
      {
        test: /images\/[^.]+\.(svg|png|jpg|jpeg)$/,
        loader: 'file?name=/images/[hash].[ext]',
        exclude: /node_modules/,
      },
    ],
  },
  postcss: [
    autoprefixer({
      browsers: [
        'last 30 versions',
      ],
    })
  ],
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractTextPlugin('/css/[name].css'),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
  ],
  devtool: 'eval',
};
