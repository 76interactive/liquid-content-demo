<?php
  $u = new User();
  $c = Page::getCurrentPage();
?>
<!doctype html>
<html lang="nl">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <?php Loader::element('header_required') ?>
<!--  <link href="<?php echo $view->getThemePath() ?>/dist/css/app.css" type="text/css" rel="stylesheet" />-->
  <link href="<?php echo $view->getThemePath() ?>/assets/css/style.css" type="text/css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<?php echo $view->getThemePath() ?>/assets/js/app.js" type="text/javascript"></script>
  <!-- Start Visual Website Optimizer Asynchronous Code -->
  <script type='text/javascript'>
  var _vwo_code=(function(){
  var account_id=279074,
  settings_tolerance=2000,
  library_tolerance=2500,
  use_existing_jquery=false,
  /* DO NOT EDIT BELOW THIS LINE */
  f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
  </script>
  <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body<?php if ($u->isLoggedIn()):?> style="margin-top: 48px;"<?php endif; ?>>

  <div class="page"><!-- Start Page -->
    <div class="page__wrap" id="top"><!-- Start Page Wrap -->

      <!-- Header -->
      
      <header class="header header--fixed background__black--transparent">
        <div class="grid__container">
          <div class="grid__row">
            <div class="grid__col--12 header__logo">
              <?php
                $a = new GlobalArea('Logo');
                $a->display($c);
              ?>
            </div>
            <div class="grid__col--12 header__title">
              <h1>Liquid content demo</h1>
            </div>
            <div class="grid__col--12 header__navicon">
                <a class="navicon x" href="#">
                  <div class="navicon__bars"></div>
                </a>
              </div>
            <div class="grid__col--12">
              <nav class="header__nav" <?php if ($c->isEditMode()): ?>style="width: 100%;"<?php endif; ?>>
                <?php
                  $a = new GlobalArea('Navigation');
                  $a->display($c);
                ?>
              </nav>
              
            </div>
          </div>
        </div>
      </header>
      
      <section class="hero">
            <?php
              $a = new Area('Hero');
              $a->display($c);
            ?>
      </section>

