<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php if ($control->getHeadline()) { ?>
  <h2><?php echo $control->getHeadline()?></h2>
<?php } ?>
<?php if ($control->getBody()) { ?>
  <p><?php echo $control->getBody()?></p>
<?php } ?>
