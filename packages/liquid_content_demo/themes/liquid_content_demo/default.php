<?php
  Loader::packageElement('header', 'liquid_content_demo');
  $image = $c->getAttribute('page_image');
  if ($image) {
    $image_src = $image->getRelativePath();
    $image_title = $image->getTitle();
  } else {
    $image_src = $view->getThemePath() . '/assets/images/placeholder.png';
  }
?>

  <section class="section section__intro">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--8 grid__col--sm--12">
          <?php
            $a = new Area('Intro');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__first-demo" id="first">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--6 grid__col--sm--12 left">
          <?php
            $a = new Area('First demo intro');
            $a->display($c);
          ?>
        </div>
        
        <div class="grid__col--6 grid__col--sm--12 right">
          <?php
            $a = new Area('First demo');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__second-demo" id="second">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--6 grid__col--sm--12 left">
          <?php
            $a = new Area('Second demo intro');
            $a->display($c);
          ?>
        </div>
        
        <div class="grid__col--6 grid__col--sm--12 right">
          <?php
            $a = new Area('Second demo');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__third-demo" id="third">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--6 grid__col--sm--12 left">
          <?php
            $a = new Area('Third demo intro');
            $a->display($c);
          ?>
        </div>
        
        <div class="grid__col--6 grid__col--sm--12 right">
          <?php
            $a = new Area('Third demo');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__fourth-demo" id="fourth">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--6 grid__col--sm--12 left">
          <?php
            $a = new Area('Fourth demo intro');
            $a->display($c);
          ?>
        </div>
        
        <div class="grid__col--6 grid__col--sm--12 right">
          <?php
            $a = new Area('Fourth demo');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__fifth-demo" id="fifth">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--6 grid__col--sm--12 left">
          <?php
            $a = new Area('Fifth demo intro');
            $a->display($c);
          ?>
        </div>
        
        <div class="grid__col--6 grid__col--sm--12 right">
          <?php
            $a = new Area('Fifth demo');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__sixth-demo" id="sixth">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--6 grid__col--sm--12 left">
          <?php
            $a = new Area('Sixth demo intro');
            $a->display($c);
          ?>
        </div>
        
        <div class="grid__col--6 grid__col--sm--12 right">
          <?php
            $a = new Area('Sixth demo');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section section__tips" id="tips">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--12 grid__col--sm--12">
          <?php
            $a = new Area('Tips');
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </section>

<?php Loader::packageElement('footer', 'liquid_content_demo'); ?>
